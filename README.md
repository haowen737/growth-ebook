Growth电子书版
===

> 这个项目是开源Web学习应用[Growth](https://github.com/phodal/growth)的电子书版本。

这是继之前的《[教你设计物联网系统](https://github.com/phodal/designiot)》与《[GitHub 漫游指南](https://github.com/phodal/github-roam)》后的电子书，也是还在编写的《[RePractise](https://github.com/phodal/repractise)》的前传。 Growth是面向初中级Web开发者，而Repractise将面向中高级Web开发者。

本书是依据Web软件开发的迭代过程而编写的，主要内容如下:  

 - 基础知识篇
 - 前端与后台
 - 编码
 - 上线
 - 数据分析
 - 持续交付
 - 遗留系统
 - 回顾与新架构

其中一些内容来自于博客，一些内容还在编写中，欢迎提交指导意见。

应用下载
---

<a href="https://play.google.com/store/apps/details?id=ren.growth&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-AC-global-none-all-co-pr-py-PartBadges-Oct1515-1"><img alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/apps/en-play-badge-border.png" width="180"/></a>
<a href="http://windowsphone.com/s?appid=a6022e5d-b101-4d8f-a836-d3bfb6fd73ef"><img src="https://assets.windowsphone.com/8d997df3-5b6e-496c-88b3-e3f40117c8fd/English-get-it-from-MS_InvariantCulture_Default.png" srcset="https://assets.windowsphone.com/0100c7e5-1455-40bc-a351-b77d2801ad5a/English_get-it-from-MS_InvariantCulture_Default.png 2x"  alt="Get it from Microsoft" /></a>
<a href="https://itunes.apple.com/cn/app/growth/id1078807522?l=zh&ls=1&mt=8"><img src="https://raw.githubusercontent.com/phodal/growth/master/docs/apple.png" width="180" alt="Get it from Apple Store" /></a>


Donate
---

(ps: For Better Server & Tech Support)

有钱捧个钱场:

![Alipay](https://raw.githubusercontent.com/phodal/growth/master/docs/alipay.png)![Wechat](https://raw.githubusercontent.com/phodal/growth/master/docs/wechat.png)


没钱捧个人场(关注我的微信公众号):

![QRCode](https://raw.githubusercontent.com/phodal/growth/master/www/img/wechat.jpg)

License
---

© 2015~2016 [Phodal Huang](https://www.phodal.com). This code is distributed under the CC0 1.0 Universal license. See `LICENSE` in this directory.

[待我代码编成，娶你为妻可好](http://www.xuntayizhan.com/person/ji-ke-ai-qing-zhi-er-shi-dai-wo-dai-ma-bian-cheng-qu-ni-wei-qi-ke-hao-wan/)
